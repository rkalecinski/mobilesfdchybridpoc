//
//  BaseViewController.m
//  SFDCMobileSDKNative
//
//  Created by Robert Kalecinski on 24.10.2013.
//  Copyright (c) 2013 Roche. All rights reserved.
//

#import "BaseViewController.h"

@interface BaseViewController ()

@end

@implementation BaseViewController

- (BOOL)showsRevealButton
{
    return NO;
}

- (BOOL)allowsDragToReveal
{
    return NO;
}

- (void)revealSidebar
{
    [APP_DELEGATE.revealController toggleSidebar:!APP_DELEGATE.revealController.sidebarShowing
                                        duration:kGHRevealSidebarDefaultAnimationDuration];
}

- (void)didPan:(UIPanGestureRecognizer*)gr
{
    if(!self.allowsDragToReveal) return;
    
    APP_DELEGATE.revealController->_sidebarView.transform = CGAffineTransformIdentity;
    [APP_DELEGATE.revealController dragContentView:gr];
}

- (void)menuDidReveal
{
    UIView* draggingView = [[UIView alloc]initWithFrame:self.view.frame];
    draggingView.tag = 666;
    [self.view addSubview:draggingView];
    
    draggingView.backgroundColor = [UIColor redColor];
    draggingView.alpha = 0.5f;
    
    UIPanGestureRecognizer* panGr = [[UIPanGestureRecognizer alloc]initWithTarget:self
                                                                           action:@selector(didPan:)];
    [draggingView addGestureRecognizer:panGr];
}

- (void)menuDidHide
{
    UIView* draggingView = [self.view viewWithTag:666];
    [draggingView removeFromSuperview];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if(self.showsRevealButton)
    {
        UIImage *btnImage = [UIImage imageNamed:@"menu"];
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        btn.bounds = CGRectMake(5., 0., btnImage.size.width, btnImage.size.height );
        [btn addTarget:self action:@selector(revealSidebar) forControlEvents:UIControlEventTouchUpInside];
        [btn setImage:btnImage forState:UIControlStateNormal];
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btn];
    }
    
    if(self.allowsDragToReveal)
    {
        UIPanGestureRecognizer* panGr = [[UIPanGestureRecognizer alloc]initWithTarget:self
                                                                               action:@selector(didPan:)];
        [self.navigationController.navigationBar addGestureRecognizer:panGr];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
