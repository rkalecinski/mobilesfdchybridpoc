//
//  MapViewController.m
//  SFDCMobileSDKNative
//
//  Created by Robert Kalecinski on 24.10.2013.
//  Copyright (c) 2013 Roche. All rights reserved.
//

#import "MapViewController.h"

@interface MapViewController ()

@end

@implementation MapViewController

- (BOOL)showsRevealButton
{
    return YES;
}

- (BOOL)allowsDragToReveal
{
    return YES;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIView *iv = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 250, 32)];
    UITextField *txtSearch = [[UITextField alloc]initWithFrame:CGRectMake(0, 0, iv.frame.size.width, iv.frame.size.height)];
    txtSearch.placeholder = @"Search...";
    [iv addSubview:txtSearch];
    [iv setBackgroundColor:[UIColor clearColor]];
    self.navigationItem.titleView = iv;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
