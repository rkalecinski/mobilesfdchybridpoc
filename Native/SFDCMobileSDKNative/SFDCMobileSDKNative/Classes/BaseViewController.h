//
//  BaseViewController.h
//  SFDCMobileSDKNative
//
//  Created by Robert Kalecinski on 24.10.2013.
//  Copyright (c) 2013 Roche. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseViewController : UIViewController

- (BOOL)showsRevealButton;
- (BOOL)allowsDragToReveal;
- (void)menuDidReveal;
- (void)menuDidHide;

@end
